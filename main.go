package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"gitlab.com/controllercubiomes/util"
	"log"
	"net/http"
	"os"
	"strconv"
)

const url = "http://sevisapickyasshole.net/api"

func toFloat(s string) (f float32) {
	t, err := strconv.ParseFloat(s[:len(s)-1], 32)
	if err != nil {
		log.Fatal(err)
	}
	f = float32(t)
	return
}

func toInt64(s string) (i int64) {
	i, _ = strconv.ParseInt(s, 10, 64)
	return
}

func main() {
	lines, err := ReadCsv("found.csv")
	if err != nil {
		panic(err)
	}

	// Loop through lines & turn into object
	//var seeds []util.Seed
	for _, line := range lines[1:] {
		seed := util.Seed{
			Finder:                           "",
			Seed:                             toInt64(line[0]),
			Badlands:                         toFloat(line[1]),
			Badlands_plateau:                 toFloat(line[2]),
			Bamboo_jungle:                    toFloat(line[3]),
			Bamboo_jungle_hills:              toFloat(line[4]),
			Basalt_deltas:                    toFloat(line[5]),
			Beach:                            toFloat(line[6]),
			Birch_forest:                     toFloat(line[7]),
			Birch_forest_hills:               toFloat(line[8]),
			Cold_ocean:                       toFloat(line[9]),
			Crimson_forest:                   toFloat(line[10]),
			Dark_forest:                      toFloat(line[11]),
			Dark_forest_hills:                toFloat(line[12]),
			Deep_cold_ocean:                  toFloat(line[13]),
			Deep_frozen_ocean:                toFloat(line[14]),
			Deep_lukewarm_ocean:              toFloat(line[15]),
			Deep_ocean:                       toFloat(line[16]),
			Deep_warm_ocean:                  toFloat(line[17]),
			Desert:                           toFloat(line[18]),
			Desert_hills:                     toFloat(line[19]),
			Desert_lakes:                     toFloat(line[20]),
			End_barrens:                      toFloat(line[21]),
			End_highlands:                    toFloat(line[22]),
			End_midlands:                     toFloat(line[23]),
			Eroded_badlands:                  toFloat(line[24]),
			Flower_forest:                    toFloat(line[25]),
			Forest:                           toFloat(line[26]),
			Frozen_ocean:                     toFloat(line[27]),
			Frozen_river:                     toFloat(line[28]),
			Giant_spruce_taiga:               toFloat(line[29]),
			Giant_spruce_taiga_hills:         toFloat(line[30]),
			Giant_tree_taiga:                 toFloat(line[31]),
			Giant_tree_taiga_hills:           toFloat(line[32]),
			Gravelly_mountains:               toFloat(line[33]),
			Ice_spikes:                       toFloat(line[34]),
			Jungle:                           toFloat(line[35]),
			Jungle_edge:                      toFloat(line[36]),
			Jungle_hills:                     toFloat(line[37]),
			Lukewarm_ocean:                   toFloat(line[38]),
			Modified_badlands_plateau:        toFloat(line[39]),
			Modified_gravelly_mountains:      toFloat(line[40]),
			Modified_jungle:                  toFloat(line[41]),
			Modified_jungle_edge:             toFloat(line[42]),
			Modified_wooded_badlands_plateau: toFloat(line[43]),
			Mountain_edge:                    toFloat(line[44]),
			Mountains:                        toFloat(line[45]),
			Mushroom_fields:                  toFloat(line[46]),
			Mushroom_field_shore:             toFloat(line[47]),
			Nether_wastes:                    toFloat(line[48]),
			Ocean:                            toFloat(line[49]),
			Plains:                           toFloat(line[50]),
			River:                            toFloat(line[51]),
			Savanna:                          toFloat(line[52]),
			Savanna_plateau:                  toFloat(line[53]),
			Shattered_savanna:                toFloat(line[54]),
			Shattered_savanna_plateau:        toFloat(line[55]),
			Small_end_islands:                toFloat(line[56]),
			Snowy_beach:                      toFloat(line[57]),
			Snowy_mountains:                  toFloat(line[58]),
			Snowy_taiga:                      toFloat(line[59]),
			Snowy_taiga_hills:                toFloat(line[60]),
			Snowy_taiga_mountains:            toFloat(line[61]),
			Snowy_tundra:                     toFloat(line[62]),
			Soul_sand_valley:                 toFloat(line[63]),
			Stone_shore:                      toFloat(line[64]),
			Sunflower_plains:                 toFloat(line[65]),
			Swamp:                            toFloat(line[66]),
			Swamp_hills:                      toFloat(line[67]),
			Taiga:                            toFloat(line[68]),
			Taiga_hills:                      toFloat(line[69]),
			Taiga_mountains:                  toFloat(line[70]),
			Tall_birch_forest:                toFloat(line[71]),
			Tall_birch_hills:                 toFloat(line[72]),
			The_end:                          toFloat(line[73]),
			The_void:                         toFloat(line[74]),
			Warm_ocean:                       toFloat(line[75]),
			Warped_forest:                    toFloat(line[76]),
			Wooded_badlands_plateau:          toFloat(line[77]),
			Wooded_hills:                     toFloat(line[78]),
			Wooded_mountains:                 toFloat(line[79]),
		}
		//fmt.Println(data.Column1 + " " + data.Column2)
		data, err := json.Marshal(seed)
		if err != nil {
			log.Fatal(err)
		}
		req, err := http.NewRequest("POST", url+"/seed", bytes.NewBuffer(data))
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("authorization", "test")
		client := &http.Client{}
		_, err = client.Do(req)
		if err != nil {
			log.Println(err)
		}
	}
}

// ReadCsv accepts a file and returns its content as a multi-dimentional type
// with lines and each column. Only parses to string type.
func ReadCsv(filename string) ([][]string, error) {

	// Open CSV file
	f, err := os.Open(filename)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	// Read File into a Variable
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return [][]string{}, err
	}

	return lines, nil
}
